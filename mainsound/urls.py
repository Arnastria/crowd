from django.urls import re_path
from .views import *


#url for app
urlpatterns = [
    re_path(r'^profile/',view_profile,name='profile'),
    re_path(r'^status-submit/',submit_status,name='submit_status'),
    re_path('',view_home,name = 'redirect'),

]

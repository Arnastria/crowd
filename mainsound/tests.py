from django.test import TestCase, Client,LiveServerTestCase
from datetime import datetime
from django.urls import resolve, reverse
from .views import *
from .models import StatusModel
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class CrowdMainSoundUnitTest(TestCase):

    #for creating object model
    def setUpObject(self):
        return StatusModel.objects.create(crowder="Rui", message="Gatchagatcha",time= datetime.datetime.now())

    #test to get response
    def test_home_url_is_exist(self):
        response = Client().get('//')
        self.assertEqual(response.status_code,200)

    #test the view
    def test_home_correct_views(self):
        found = resolve('//')
        self.assertEqual(found.func,view_home)

    #test if home without status still giving response
    def test_home_without_status(self):
        StatusModel.objects.all().delete()
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    #test creating new objects
    def test_model_can_create_new_status(self):
        new_status = self.setUpObject()
        all_stats = StatusModel.objects.all().count()
        self.assertEqual(all_stats,1)

    #test creating form with fillings
    def test_form_is_valid(self):
        new_form = StatusForm(
            data={'crowder': 'Rui', 'message': 'GatchaDance'})
        self.assertTrue(new_form.is_valid())

    #test creating form and check the class
    def test_form_has_css_class(self):
        new_form = StatusForm()
        self.assertIn('class="form-control', new_form.as_p())

    #test creating invalid form
    def test_form_validation_for_blankInput(self):
        new_form = StatusForm(data={'crowder':'','message':''})

        self.assertTrue(new_form.fields['message'].max_length == 300)
        self.assertFalse(new_form.is_valid())

    #test if the content of object is valid
    def test_model_content_is_valid(self):
        new_status = self.setUpObject()
        self.assertEqual(new_status.crowder, 'Rui')
        self.assertEqual(new_status.message, 'Gatchagatcha')

    #test if the model can be printed
    def test_model_can_print(self):
        new_status = self.setUpObject()
        self.assertEqual('Rui', new_status.__str__())


    def test_home_page_contains_correct_html(self):
        response = self.client.get('//')
        self.assertContains(response, '<h1>Welcome to The Crowds</h1>')
        self.assertContains(response, '<button id="submit_btn" type = "submit" class = "btn btn-primary">')
        self.assertContains(response, '<div class="wrapper-form">')
        self.assertContains(response, 'Hello apa kabar?')

    def test_home_post_success_and_render_result(self):
        response_status = Client().post(reverse('submit_status'),
                                        {'crowder': 'Rui', 'message': 'GatchaDance'})
        self.assertEqual(response_status.status_code, 302)

        response = Client().get('//')
        html_response = response.content.decode('utf8')
        self.assertIn('Rui', html_response)

    def test_post_failure_and_render_result(self):
        response_status = Client().post(reverse('submit_status'), {'name': '', 'status': ''})
        self.assertEqual(response_status.status_code, 302)

        response = Client().get('//')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Anonim', html_response)

class CrowdProfileTest (TestCase):
    def test_routing_url(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_view_profile(self):
        response = resolve('/profile/')
        self.assertEqual(response.func,view_profile)

    def test_profile_correct_html(self):
        response = Client().get('/profile/')

        self.assertContains(response,"Arnastria")
        self.assertContains(response,"28 Sept")
        self.assertContains(response, "Developer")
        self.assertContains(response,"1706027414")
        self.assertContains(response, "created this webapp for my TDD lecture using Django")

class CrowdFunctionalTest (LiveServerTestCase):

    '''
    For testing to the real webApp use : selenium.get("https://crowd.herokuapp.com") instead of the self.live_server_url
    and change the extended class from LiveServerTestCase to TestCase
    '''

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
    
    def tearDown(self):
        self.selenium.quit()
        super(CrowdFunctionalTest, self).tearDown()

    def test_input(self):
        selenium = self.selenium

        selenium.get(self.live_server_url)
        crowder = selenium.find_element_by_id("id_crowder")
        message = selenium.find_element_by_id("id_message")

        submit = selenium.find_element_by_id("submit_btn")

        crowder.send_keys("Arnastria")
        message.send_keys("Lets use TDD together!11!! Coba Coba")


        submit.send_keys(Keys.RETURN)
        assert "Arnastria" in selenium.page_source
        assert "Coba Coba" in selenium.page_source

    def test_layout_mainPage(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        title_h1 = selenium.find_element_by_id("crowds").find_element_by_css_selector("h1").text

        self.assertIn("Welcome to The Crowds",title_h1)

    def test_layout_profile(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/profile/")
        profile_name = selenium.find_element_by_id("profile_cont_fluid").find_element_by_class_name("wrapper-profile").find_element_by_css_selector("h2").text

        self.assertIn("Arnastria",profile_name)

    def test_style_mainPage(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        button_toProfile = selenium.find_element_by_id("btn-toProfile").get_attribute("class")
        self.assertEqual("btn btn-primary",button_toProfile)


    def test_style_profile(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/profile/")

        button_toMain = selenium.find_element_by_id("btn-toMain").get_attribute("class")
        self.assertEqual("btn btn-primary",button_toMain)

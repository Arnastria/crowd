from django.shortcuts import render
import datetime
from django.http import HttpResponseRedirect
from .models import StatusModel
from .forms import StatusForm

# Create your views here.
response ={}

def view_home(request):

    if(StatusModel.objects.all().count() !=0):
        return render(request, 'home.html',{'Form' : StatusForm,'status_list' : StatusModel.objects.all()})
    else:
        return render(request, 'home.html',{'Form' :StatusForm})

def view_profile(request):
    return render(request,'profile.html')

def submit_status(request):
    form = StatusForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['crowder'] = request.POST['crowder'] if request.POST['crowder'] != "" else "No Sender"
        response['message'] = request.POST['message'] if request.POST['message'] != "" else "No Message"
        response['time'] = datetime.datetime.now()
        status = StatusModel(crowder=response['crowder'],
                             message=response['message'],time = response['time'])

        status.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
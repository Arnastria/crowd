# Generated by Django 2.1.1 on 2018-10-10 18:34

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('mainsound', '0002_remove_statusmodel_to'),
    ]

    operations = [
        migrations.AddField(
            model_name='statusmodel',
            name='time',
            field=models.TimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]

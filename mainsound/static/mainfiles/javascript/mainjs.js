$(document).ready(function(){
    $('#theme-checkbox01').click(function() {
        if ($(this).is(':checked')) {
          dark01()
        } else {
          light01()
        }
      })

    $('#theme-checkbox02').click(function() {
        if ($(this).is(':checked')) {
          dark02()
        } else {
          light02()
        }
      })

      function dark01(){
        $('body').animate({
            'background-color': '#2D2D2D',
            'color': '#ffffff'
        })
        $('.message-title').animate({
            'background-color': '#c82333'
        })
        $('#btn-toProfile').removeClass("btn btn-primary")
        .addClass("btn btn-danger");
        $('#submit_btn').removeClass("btn btn-primary")
        .addClass("btn btn-danger");
      }

      function light01() {
        $('body').animate({
            'background-color': '#ffffff',
            'color' : '#000000'
        });
        $('.message-title').animate({
            'background-color': '#007bff'
        })
        $('#btn-toProfile').removeClass("btn btn-danger")
        .addClass("btn btn-primary");
        $('#submit_btn').removeClass("btn btn-danger")
        .addClass("btn btn-primary");
      }

      function dark02(){
        $('body').animate({
            'background-color': '#2D2D2D',
            'color': '#ffffff'
        })
        $('#btn-toMain').removeClass("btn btn-primary")
        .addClass("btn btn-danger");
      }

      function light02() {
        $('body').animate({
            'background-color': '#ffffff',
            'color' : '#000000'
        })
        $('#btn-toMain').removeClass("btn btn-danger")
        .addClass("btn btn-primary");
      }
    
    $( function() {
        $( "#accordion" ).accordion({
            icons:null,
            collapsible: true,
            active: false,
            heightStyle: "content"
        });
    } );
});